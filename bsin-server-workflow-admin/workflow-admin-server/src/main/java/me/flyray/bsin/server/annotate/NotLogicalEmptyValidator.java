package me.flyray.bsin.server.annotate;
import me.flyray.bsin.server.util.ExtOptional;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author zsk
 * 2021/4/20 19:35
 */
public class NotLogicalEmptyValidator implements ConstraintValidator<NotLogicalEmpty,Object> {
    @Override
    public void initialize(NotLogicalEmpty constraintAnnotation) {

    }

    @Override
    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        return !ExtOptional.isLogicalEmptyObject(o);
    }
}
