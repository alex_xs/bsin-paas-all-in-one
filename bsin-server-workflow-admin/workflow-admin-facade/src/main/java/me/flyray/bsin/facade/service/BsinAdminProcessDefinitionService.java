package me.flyray.bsin.facade.service;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.Map;

@Path("processDefinition")
public interface BsinAdminProcessDefinitionService {

    /**
     * 查询最新版本的流程定义列表
     */
    @POST
    @Path("getProcessDefinitionPageList")
    @Produces("application/json")
    Map<String, Object> getProcessDefinitionPageList(Map<String, Object> requestMap);
}
