package me.flyray.bsin.facade.service;

import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

/**
* @author bolei
* @description 针对表【sys_product】的数据库操作Service
* @createDate 2023-11-05 17:59:52
*/

@Path("product")
public interface ProductService{

    /**
     * 新增
     */
    @POST
    @Path("add")
    @Produces("application/json")
    public Map<String, Object> add(Map<String, Object> requestMap);

    /**
     * 删除
     */
    @POST
    @Path("delete")
    @Produces("application/json")
    public Map<String, Object> delete(Map<String, Object> requestMap);

    /**
     * 更新
     */
    @POST
    @Path("edit")
    @Produces("application/json")
    public Map<String, Object> edit(Map<String, Object> requestMap);

    /**
     * 根据条件查询列表
     */
    @POST
    @Path("getList")
    @Produces("application/json")
    public Map<String, Object> getList(Map<String, Object> requestMap);

    /**
     * 根据条件查询列表
     */
    @POST
    @Path("getPageList")
    @Produces("application/json")
    public Map<String, Object> getPageList(Map<String, Object> requestMap);

    /**
     * 添加产品对应的应用
     */
    @POST
    @Path("addProductApp")
    @Produces("application/json")
    public Map<String, Object> addProductApp(Map<String, Object> requestMap);

    /**
     * 删除产品与应用的对应关系
     */
    @POST
    @Path("deleteProductApp")
    @Produces("application/json")
    public Map<String, Object> deleteProductApp(Map<String, Object> requestMap);

    /**
     * 查询产品对应的应用列表
     */
    @POST
    @Path("getProductAppList")
    @Produces("application/json")
    public Map<String, Object> getProductAppList(Map<String, Object> requestMap);

    /**
     * 查询产品对应的应用及功能列表
     */
    @POST
    @Path("getProductAppFunctionList")
    @Produces("application/json")
    public Map<String, Object> getProductAppFunctionList(Map<String, Object> requestMap);

    /**
     * 查询产品对应的应用列表
     */
    @POST
    @Path("getProductAppPageList")
    @Produces("application/json")
    public Map<String, Object> getProductAppPageList(Map<String, Object> requestMap);

}
