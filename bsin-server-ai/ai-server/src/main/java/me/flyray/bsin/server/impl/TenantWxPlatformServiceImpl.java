package me.flyray.bsin.server.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Map;

import cn.hutool.crypto.symmetric.SymmetricAlgorithm;
import cn.hutool.crypto.symmetric.SymmetricCrypto;
import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.context.BsinServiceContext;
import me.flyray.bsin.exception.BusinessException;
import me.flyray.bsin.facade.service.TenantWxPlatformService;
import me.flyray.bsin.server.domain.TenantWxPlatform;
import me.flyray.bsin.server.mapper.AiTenantWxPlatformMapper;
import me.flyray.bsin.utils.BsinSnowflake;
import me.flyray.bsin.utils.RespBodyHandler;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp】的数据库操作Service实现
* @createDate 2023-04-25 18:41:19
*/
@Service
public class TenantWxPlatformServiceImpl implements TenantWxPlatformService {

    @Autowired
    private AiTenantWxPlatformMapper tenantWxmpMapper;
    @Value("${bsin.ai.aesKey}")
    private String aesKey;

    @Override
    public Map<String, Object> add(Map<String, Object> requestMap) {
        TenantWxPlatform tenantWxPlatform = BsinServiceContext.getReqBodyDto(TenantWxPlatform.class, requestMap);
        TenantWxPlatform tenantWxmpResult = tenantWxmpMapper.selectByAppId(tenantWxPlatform.getAppId());
        if(tenantWxmpResult != null){
            throw new BusinessException("","");
        }
        String bizAppId = (String)requestMap.get("bizAppId");
        String serialNo = BsinSnowflake.getId();
        tenantWxPlatform.setAppId(bizAppId);
        tenantWxPlatform.setSerialNo(serialNo);

        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        tenantWxPlatform.setAppSecret(aes.encryptHex(tenantWxPlatform.getAppSecret()));
        tenantWxmpMapper.insert(tenantWxPlatform);
        return RespBodyHandler.setRespBodyDto(tenantWxPlatform);
    }

    @Override
    public Map<String, Object> delete(Map<String, Object> requestMap) {
        String serialNo = (String)requestMap.get("serialNo");
        // 删除
        tenantWxmpMapper.deleteById(serialNo);
        return RespBodyHandler.RespBodyDto();
    }

    @Override
    public Map<String, Object> edit(Map<String, Object> requestMap) {
        TenantWxPlatform tenantWxPlatform = BsinServiceContext.getReqBodyDto(TenantWxPlatform.class, requestMap);
        String tenantId = (String)requestMap.get("tenantId");
        String bizAppId = (String)requestMap.get("bizAppId");
        TenantWxPlatform tenantWxmpResult = tenantWxmpMapper.selectByTenantId(tenantId);
        if(tenantWxmpResult == null){
            throw new BusinessException(ResponseCode.APP_CODE_EXISTS);
        }
        tenantWxPlatform.setAppId(bizAppId);
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        tenantWxPlatform.setAppSecret(aes.encryptHex(tenantWxPlatform.getAppSecret()));
        tenantWxPlatform.setSerialNo(tenantWxmpResult.getSerialNo());
        tenantWxmpMapper.updateById(tenantWxPlatform);
        return RespBodyHandler.setRespBodyDto(tenantWxPlatform);
    }

    @Override
    public Map<String, Object> detail(Map<String, Object> requestMap) {
        String tenantId = (String)requestMap.get("tenantId");
        TenantWxPlatform tenantWxPlatform = tenantWxmpMapper.selectByTenantId(tenantId);
        SymmetricCrypto aes = new SymmetricCrypto(SymmetricAlgorithm.AES, aesKey.getBytes());
        tenantWxPlatform.setAppSecret(aes.decryptStr(tenantWxPlatform.getAppSecret()));
        return RespBodyHandler.setRespBodyDto(tenantWxPlatform);
    }

}
