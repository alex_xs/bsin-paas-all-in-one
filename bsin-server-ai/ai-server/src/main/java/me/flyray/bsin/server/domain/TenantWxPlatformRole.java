package me.flyray.bsin.server.domain;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

/**
 * 
 * @TableName ai_tenant_wxmp_role
 */
@Data
public class TenantWxPlatformRole {
    /**
     * 
     */
    private String serialNo;

    /**
     * 所属租户
     */
    private String tenantId;

    /**
     * 微信账号ID
     */
    private String appType;

    /**
     * 级别
     */
    private String level;

    /**
     * 角色描述
     */
    private String content;

    /**
     * 
     */
    private Date createTime;

    /**
     * 
     */
    private Date updateTime;

}