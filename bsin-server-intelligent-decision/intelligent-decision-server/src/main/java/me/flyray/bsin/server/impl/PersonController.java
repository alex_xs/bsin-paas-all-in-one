package me.flyray.bsin.server.impl;

import org.kie.api.KieBase;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.bsin.server.domain.Person;

@RestController
public class PersonController {

    private final KieContainer kieContainer;

    public PersonController(KieContainer kieContainer) {
        this.kieContainer = kieContainer;
    }

    /**
     * 调用drools rule判断用户是否可以玩游戏
     */
    @GetMapping("canPlayGame")
    public Person canPlayGame(Person person) {
        //KieBase kieBase = kieContainer.getKieBase("kabse");
        //KieSession kieSession = kieBase.newKieSession();
        KieSession kieSession = kieContainer.newKieSession("ksession");
        try {
            // 将输入数据提供给规则引擎
            kieSession.insert(person);
            // 激活规则引擎，如果规则匹配成功则执行规则
            kieSession.fireAllRules();
        } finally {
            kieSession.dispose();
        }
        return person;
    }
}